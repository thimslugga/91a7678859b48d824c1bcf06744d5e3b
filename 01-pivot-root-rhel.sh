#!/bin/bash
#
# This script pivots the root partition to a tmpfs mount point, so that the root volume can be re-partitioned.
#
# Note: Ensure that the host has enough memory to be able to move the rootfs to memory.
#
# Related Resources:
#
# - https://unix.stackexchange.com/a/126222
# - https://web.archive.org/web/20160730094856/http://wiki.sourcemage.org/HowTo(2f)Initramfs.html
# - http://www.ivarch.com/blogs/oss/2007/01/resize-a-live-root-fs-a-howto.shtml

#set -x
#set -e

# Check if this script was run as a non-root user
function is_root() {
  if [[ ${EUID} -ne 0 ]]; then
    echo "This script must be run as root."
    exit 1
  fi
}
#is_root

function rhel_install_prereqs() {
  # Install pre-requisites
  command -v ps >/dev/null 2>&1 || dnf install -y psmisc
  command -v lsof >/dev/null 2>&1 || dnf install -y lsof
  
  dnf list installed dosfstools >/dev/null 2>&1 || dnf install -y dosfstools
  dnf list installed grubby >/dev/null 2>&1 || dnf install -y grubby
  dnf list installed plymouth >/dev/null 2>&1 || dnf install -y plymouth
}

function save_mounted_filesystems() {
  df -TH > /root/mounted_fs.out
}

function rhel_configure_grub() {
  grubby --update-kernel=ALL --args="console=tty0 console=ttyS0,115200n8"
}

function ec2_configure_dns() {
  echo -e "search ec2.internal\nnameserver 169.254.169.253\n" | tee /etc/resolv.conf
}

function mknewroot() {
  install -Ddm 000755 /newroot
  mount -t tmpfs tmpfs /newroot
  #mount -t tmpfs none /newroot
}

function mkoldroot() {
  mkdir -pv /newroot/oldroot
}

function cp_to_newroot() {
  cp -ax / /newroot;
  cp -a /dev /newroot;
}

# systemd now mounts the root fs as 'shared' by default
# https://bugzilla.redhat.com/show_bug.cgi?id=1361043
# https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/fs/namespace.c?h=v3.10#n2627
function remount_root() {
  # Alternative way of doing this
  #grep -iP '/ /\s' /proc/$$/mountinfo
  #unshare -m
  # Preferred way of doing this
  mount --make-rprivate /
}

function unmount_all() {
  umount -a || true
}

function unmount_varlibnfsrpc() {
  if systemctl is-active --quiet var-lib-nfs-rpc_pipefs.mount; then 
    systemctl stop var-lib-nfs-rpc_pipefs.mount;
  fi
}

# Prevent SELinux from interfering with this process
function disable_selinux() {
  setenforce 0 || true
  sestatus
}

# The auditd service cannot be stopped via systemctl in RHEL 7 and later.
#
# Note: RHEL7 and later don't accept systemctl stop/start/restart auditd.service because the service 
# definition has RefuseManualStop=yes. Due to that, you need to run service auditd restart for 
# restarting the daemon in RHEL7 and later.
#
# Note: The reason for this unusual handling of restart/stop requests is that auditd is treated specially 
# by the kernel: the credentials of a process that sends a killing signal to auditd are saved to the 
# audit log. The audit developers do not want to see the credentials of PID 1 logged there. They want 
# to see the login UID of the user who initiated the action.
#
# https://access.redhat.com/solutions/2664811
# https://access.redhat.com/solutions/1240243
# https://access.redhat.com/solutions/44602
# https://bugzilla.redhat.com/show_bug.cgi?id=1026648
# https://docs.fedoraproject.org/en-US/fedora-coreos/audit/
function stop_auditd() {
  service auditd stop
  #auditctl --signal stop
  #service auditd state
}

function upstart_stop_services() {
  local services
  telinit 2
  services=$(chkconfig --list | grep 2:on | awk '{print $1}' | grep -v -e cloud-init -e cloud-config -e cloud-final -e sshd -e network -e rawdevices)
  for service in "${services}"; do 
    service "${service}" stop || true; 
  done
}

function systemd_stop_services() {
  systemctl list-units \
    --type=service \
    --state=running \
    --no-pager \
    --no-legend \
      | awk '!/ssh/ {print $1}' \
      | xargs systemctl stop
}

function disable_firewalld() {
  if systemctl is-active --quiet firewalld; then 
    systemctl disable --now firewalld;
  fi
}

function ec2_disable_ssm_agent() {
  if systemctl is-active --quiet amazon-ssm-agent; then 
    systemctl disable --now amazon-ssm-agent; 
  fi
}

function disable_nfs_server() {
  if systemctl is-active --quiet nfs-server; then 
    systemctl disable --now nfs-server; 
  fi
}

rhel_install_prereqs

# Prevent selinux from interferring with the process
disable_selinux

# Prevent firewalld from interferring with the process
disable_firewalld

# Stop and disable misc services that could interfere with this process
ec2_disable_ssm_agent
disable_nfs_server

#upstart_stop_services
systemd_stop_services

# auditd requires special handling for us to stop the service.
stop_auditd

# This is needed to prevent DNS lookup issues
# Note: This only applies to AWS EC2 instances, adjust accordingly
ec2_configure_dns

# Now attempt to unmount as much as possible
unmount_all
unmount_varlibnfsrpc

# Create tmpfs mount at /newroot
mknewroot

# Copy everything to the tmpfs mounted at /newroot
cp_to_newroot

# Create oldroot at /newroot/oldroot
mkoldroot

remount_root

# Switch / to tmpfs at /newroot
pivot_root /newroot /newroot/oldroot

#mount none /proc -t proc
#mount none /sys -t sysfs
#mount none /dev -t devtmpfs
#mount none /dev/pts -t devpts
##mount none /selinux -t selinuxfs

mount --move /oldroot/dev /dev
mount --move /oldroot/proc /proc
mount --move /oldroot/sys /sys
mount --move /oldroot/run /run
##mount --move /oldroot/tmp /
##mount --move /oldroot/tmp /tmp || true

#lsof /oldroot
fuser -vm /oldroot
#kill -9 $(fuser -vm /oldroot 2> /fuser.log | sed "s/\b$$\b//") || true

# Unmount everything on /oldroot
for mnt in $(cat /proc/mounts | cut -d ' ' -f 2 | grep '/oldroot/' | sort -ru); do 
  umount "{mnt}" || true; 
done

# Change the default SSH port to an alternative port e.g. 2222.
# Note: Set packer to connect to port 2222, this way packer waits until this script is complete.
#sed -i -e "s/^[#]*Port .*$/Port 2222/" /etc/ssh/sshd_config
systemctl restart sshd

# Kill and reload init to free /oldroot
pkill --signal HUP sshd && systemctl daemon-reexec
#killall init && telinit u

### IMPORTANT NOTICE ###
###
### If you were disconnected, now SSH back in to the host

# Now stop all services that are using and holding /oldroot
#
# Note: DO NOT DO THIS STEP via an interactive shell because you will probably kill your session. 
#fuser -vmk /oldroot

# Instead, just use `fuser -vm /oldroot` to identify processes holding /oldroot and kill them 
# by name with `killall <name>`. Then restart ssh, reconnect to a new session, and close your 
# old session.
fuser -vm /oldroot

# /oldroot/dev needs a lazy unmount
# sometimes there may be two devtmpfs mounts
umount -l /oldroot/dev || true
umount -l /oldroot/dev || true 

# Attempt final unmount of /oldroot; this will probably fail since the process
# running this script is still open. A follow-on script or interactive shell
# can check and issue the final umount (works well with packer).
umount /oldroot/boot/efi || true
umount /oldroot || true

# Restart udevd as it's required for creation of logical volumes i.e. lv (LVM)
/sbin/start_udev

#parted -s /dev/nvme0n1 -- mktable gpt mkpart primary xfs 1049k 2m
#parted -l