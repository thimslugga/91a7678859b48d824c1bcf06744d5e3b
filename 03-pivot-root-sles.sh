#!/bin/bash

# Check if this script was run as a non-root user
function is_root()
{
  if [[ ${EUID} -ne 0 ]]; then
    echo "This script must be run as root."
    exit 1
  fi
};
#is_root

# Install pre-requisites
command -v ps >/dev/null 2>&1 || zypper in -y psmisc
command -v lsof >/dev/null 2>&1 || zypper in -y lsof
command -v mkfs.vfat >/dev/null 2>&1 || zypper in -y dosfstools

# Prevent SELinux from interfering with this process
setenforce 0 || true
sestatus || true

if systemctl is-active --quiet apparmor.service; then
  #mkdir -p /etc/systemd/system-preset
  #echo "disable apparmor.service" > /etc/systemd/system-preset/80-apparmor.preset
  systemctl disable --now apparmor.service;
  #systemctl mask apparmor.service;
fi

# Prevent firewalld from interferring with this process
if systemctl is-active --quiet firewalld; then 
  systemctl disable --now firewalld;
fi

# Stop services that are not necessary
if systemctl is-active --quiet amazon-ssm-agent; then 
  systemctl disable --now amazon-ssm-agent; 
fi

if systemctl is-active --quiet nfs-server; then 
  systemctl disable --now nfs-server; 
fi

systemctl list-units \
  --type=service \
  --state=running \
  --no-pager \
  --no-legend \
  | awk '!/ssh/ {print $1}' \
  | xargs systemctl stop

service auditd stop

cat <<EOF | tee /etc/resolv.conf

search ec2.internal
nameserver 169.254.169.253

EOF

umount -a || true

if systemctl is-active --quiet var-lib-nfs-rpc_pipefs.mount; then 
  systemctl stop var-lib-nfs-rpc_pipefs.mount;
fi

install -Ddm 000755 /newroot
mount -t tmpfs tmpfs /newroot

cp -ax / /newroot;
cp -a /dev /newroot;

mkdir -pv /newroot/oldroot

mount --make-rprivate /

pivot_root /newroot /newroot/oldroot

mount --move /oldroot/dev /dev
mount --move /oldroot/proc /proc
mount --move /oldroot/sys /sys
mount --move /oldroot/run /run

fuser -vm /oldroot

for MOUNT in $(cat /proc/mounts | cut -d ' ' -f 2 | grep '/oldroot/' | sort -ru); do umount $MOUNT || true; done

systemctl restart sshd

pkill --signal HUP sshd \
  && systemctl daemon-reexec

fuser -vmk /oldroot
  
umount -l /oldroot || true
umount /oldroot || true