# Notes

## SUSE Distribution Migration 

- https://documentation.suse.com/suse-distribution-migration-system/15/html/distribution-migration-system/index.html
- https://github.com/SUSE/suse-migration-services

## Netflix s3-flash-bootloader

- https://netflixtechblog.medium.com/datastore-flash-upgrades-187f1e4ef859
- https://github.com/Netflix-Skunkworks/s3-flash-bootloader
