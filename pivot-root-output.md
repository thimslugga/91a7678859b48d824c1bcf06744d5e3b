## pivot_root on RHEL 9.x output

## pivot_root on RHEL 8.9 output

```
[root@ip-10-10-12-84 ~]# command -v ps >/dev/null 2>&1 || dnf install -y psmisc
[root@ip-10-10-12-84 ~]# command -v lsof >/dev/null 2>&1 || dnf install -y lsof

[root@ip-10-10-12-84 ~]# setenforce 0 || true
[root@ip-10-10-12-84 ~]# sestatus
SELinux status:                 enabled
SELinuxfs mount:                /sys/fs/selinux
SELinux root directory:         /etc/selinux
Loaded policy name:             targeted
Current mode:                   permissive
Mode from config file:          enforcing
Policy MLS status:              enabled
Policy deny_unknown status:     allowed
Memory protection checking:     actual (secure)
Max kernel policy version:      33

[root@ip-10-10-12-84 ~]# if systemctl is-active --quiet firewalld; then
>   systemctl disable --now firewalld;
> fi
[root@ip-10-10-12-84 ~]# if systemctl is-active --quiet amazon-ssm-agent; then
>   systemctl disable --now amazon-ssm-agent;
> fi
[root@ip-10-10-12-84 ~]# if systemctl is-active --quiet nfs-server; then
>   systemctl disable --now nfs-server;
> fi

[root@ip-10-10-12-84 ~]# systemctl list-units \
>   --type=service \
>   --state=running \
>   --no-pager \
>   --no-legend \
>   | awk '!/ssh/ {print $1}' \
>   | xargs systemctl stop

[root@ip-10-10-12-84 ~]# service auditd stop

[root@ip-10-10-12-84 ~]# cat <<EOF > /etc/resolv.conf
> search ec2.internal
> nameserver 169.254.169.253"
> EOF

[root@ip-10-10-12-84 ~]# umount -a || true
[root@ip-10-10-12-84 ~]# if systemctl is-active --quiet var-lib-nfs-rpc_pipefs.mount; then
>   systemctl stop var-lib-nfs-rpc_pipefs.mount;
> fi

[root@ip-10-10-12-84 ~]# install -Ddm 000755 /newroot
[root@ip-10-10-12-84 ~]# mount -t tmpfs tmpfs /newroot

[root@ip-10-10-12-84 ~]# cp -ax / /newroot
[root@ip-10-10-12-84 ~]# cp -a /dev /newroot

[root@ip-10-10-12-84 ~]# mkdir -pv /newroot/oldroot

[root@ip-10-10-12-84 ~]# mount --make-rprivate /
[root@ip-10-10-12-84 ~]# pivot_root /newroot /newroot/oldroot

[root@ip-10-10-12-84 ~]# mount --move /oldroot/dev /dev
[root@ip-10-10-12-84 ~]# mount --move /oldroot/proc /proc
[root@ip-10-10-12-84 ~]# mount --move /oldroot/sys /sys
[root@ip-10-10-12-84 ~]# mount --move /oldroot/run /run

[root@ip-10-10-12-84 ~]# systemctl restart sshd

[root@ip-10-10-12-84 ~]# fuser -vm /oldroot
                     USER        PID ACCESS COMMAND
/oldroot:            root     kernel mount /oldroot
                     root          1 ...e. systemd
                     root       1105 Frce. auditd
                     root       1952 ...e. sshd
                     ec2-user   1966 ...e. sshd
                     ec2-user   1967 ..ce. bash
                     root       1994 ..ce. sudo
                     root       1996 ...e. su
                     root       1997 ...e. bash
                     root       2044 ...e. systemd-journal

[root@ip-10-10-12-84 ~]# pkill --signal HUP sshd && systemctl daemon-reexec
Connection to ip-10-10-12-84.ec2.internal closed by remote host.
Connection to ip-10-10-12-84.ec2.internal closed.

adam@workstation ~ % ssh ip-10-10-12-84.ec2.internal

[root@ip-10-10-12-84 oldroot]# fuser -vmk /oldroot
                     USER        PID ACCESS COMMAND
/oldroot:            root     kernel mount /oldroot
                     root       1105 Frce. auditd
                     root       2044 ...e. systemd-journal
                     root       2218 ..c.. bash
                     
[ec2-user@ip-10-10-12-84 ~]$ sudo umount /oldroot

[root@ip-10-10-12-84 ~]# lsblk -f
NAME    FSTYPE LABEL UUID MOUNTPOINT
nvme0n1
nvme1n1

[root@ip-10-10-12-84 ~]# findmnt -l
TARGET                    SOURCE     FSTYPE     OPTIONS
/sys                      sysfs      sysfs      rw,nosuid,nodev,noexec,relatime,seclabel
/proc                     proc       proc       rw,nosuid,nodev,noexec,relatime
/dev                      devtmpfs   devtmpfs   rw,nosuid,seclabel,size=32067956k,nr_inodes=8016989,mode=755
/dev/pts                  devpts     devpts     rw,nosuid,noexec,relatime,seclabel,gid=5,mode=620,ptmxmode=000
/run                      tmpfs      tmpfs      rw,nosuid,nodev,seclabel,mode=755
/sys/fs/cgroup            tmpfs      tmpfs      ro,nosuid,nodev,noexec,seclabel,mode=755
/sys/fs/cgroup/systemd    cgroup     cgroup     rw,nosuid,nodev,noexec,relatime,seclabel,xattr,release_agent=/usr/lib/systemd/systemd-cgroups-agent,name=systemd
/sys/fs/selinux           selinuxfs  selinuxfs  rw,relatime
/                         tmpfs      tmpfs      rw,relatime,seclabel
/proc                     none       proc       rw,relatime
/sys                      none       sysfs      rw,relatime,seclabel
/dev                      none       devtmpfs   rw,relatime,seclabel,size=32067956k,nr_inodes=8016989,mode=755
/dev/pts                  none       devpts     rw,relatime,seclabel,mode=600,ptmxmode=000
/sys/kernel/security      securityfs securityfs rw,nosuid,nodev,noexec,relatime
/dev/shm                  tmpfs      tmpfs      rw,nosuid,nodev,seclabel
/sys/fs/pstore            pstore     pstore     rw,nosuid,nodev,noexec,relatime,seclabel
/sys/firmware/efi/efivars efivarfs   efivarfs   rw,nosuid,nodev,noexec,relatime
/sys/fs/bpf               bpf        bpf        rw,nosuid,nodev,noexec,relatime,mode=700
/run/user/1000            tmpfs      tmpfs      rw,nosuid,nodev,relatime,seclabel,size=6421536k,mode=700,uid=1000,gid=1000

[root@ip-10-10-12-84 ~]# sgdisk --clear -g \
>  --new 128::+1M --typecode=128:ef02 --change-name=128:'BIOS' \
>  --new 1::+525MiB --typecode=1:ef00 --change-name=1:'EFI' \
>  --new 2::+1GiB --typecode=2:8300 --change-name=2:'BOOT' \
>  --new 3::-0 --typecode=3:8300 --change-name=3:'ROOT' \
>  /dev/nvme0n1

[root@ip-10-10-12-84 ~]# partprobe

[root@ip-10-10-12-84 ~]# lsblk -f -o +SERIAL
NAME          FSTYPE LABEL UUID                                 MOUNTPOINT SERIAL
nvme0n1                                                                    vol0f670c86686e1a054
├─nvme0n1p1   vfat         7B77-95E7
├─nvme0n1p2
├─nvme0n1p3
└─nvme0n1p128
nvme1n1                                                                    vol063f6a88d1cd464e5

[root@ip-10-10-12-84 ~]# mkfs.xfs -L BOOT -f /dev/nvme0n1p2
[root@ip-10-10-12-84 ~]# mkfs.xfs -L ROOT -f /dev/nvme0n1p3
[root@ip-10-10-12-84 ~]# fatlabel /dev/nvme0n1p1 EFI

[root@ip-10-10-12-84 ~]# lsblk -f
NAME          FSTYPE LABEL UUID                                 MOUNTPOINT
nvme0n1
├─nvme0n1p1   vfat   EFI   F01D-32E9
├─nvme0n1p2   xfs    BOOT  8f0f496e-7283-464f-8f37-ee43f2743212
├─nvme0n1p3   xfs    ROOT  c422349b-c39b-4975-aa1d-b49682e64c67
└─nvme0n1p128
nvme1n1

[root@ip-10-10-12-84 ~]# mount /dev/nvme0n1p3 /mnt
[root@ip-10-10-12-84 ~]# mount /dev/nvme0n1p2 /mnt/boot
[root@ip-10-10-12-84 ~]# mkdir -pv /mnt/boot/efi
[root@ip-10-10-12-84 ~]# mount -t vfat /dev/nvme0n1p1 /mnt/boot/efi

[root@ip-10-10-12-84 ~]# rsync -avAXP \
>   --filter='-x security.selinux' \
>   --numeric-ids \
>   --exclude='/dev/*' \
>   --exclude='/proc/*' \
>   --exclude='/sys/*' \
>   --exclude='/run/*' \
>   --exclude='/tmp/*' \
>   --exclude='/mnt/*' \
>   --exclude='/media/*' \
>   --exclude='/lost+found/' \
>   --exclude='/var/cache/yum/*' \
>   --exclude='/var/cache/dnf/*' \
>   --info=progress2 \
>   / /mnt/
```

## pivot_root SLES 15 SP4

```
ip-10-10-12-162:~ # zypper in -y psmisc lsof dosfstools

ip-10-10-12-162:~ # setenforce 0
ip-10-10-12-162:~ # sestatus || true

ip-10-10-12-162:~ # aa-status
ip-10-10-12-162:~ # systemctl disable --now apparmor.service;
Removed /etc/systemd/system/multi-user.target.wants/apparmor.service.

ip-10-10-12-162:~ # systemctl disable --now firewalld;
ip-10-10-12-162:~ # systemctl disable --now amazon-ssm-agent;
ip-10-10-12-162:~ # systemctl disable --now nfs-server;
ip-10-10-12-162:~ # systemctl list-units \
>   --type=service \
>   --state=running \
>   --no-pager \
>   --no-legend \
>   | awk '!/ssh/ {print $1}' \
>   | xargs systemctl stop

ip-10-10-12-162:~ # service auditd stop

ip-10-10-12-162:~ # systemctl stop var-lib-nfs-rpc_pipefs.mount;

ip-10-10-12-162:~ # install -Ddm 000755 /newroot
ip-10-10-12-162:~ # mount -t tmpfs tmpfs /newroot
ip-10-10-12-162:~ # cp -ax / /newroot;
ip-10-10-12-162:~ # cp -a /dev /newroot;
ip-10-10-12-162:~ # mkdir -pv /newroot/oldroot
mkdir: created directory '/newroot/oldroot'
ip-10-10-12-162:~ # mount --make-rprivate /
ip-10-10-12-162:~ #
ip-10-10-12-162:~ # pivot_root /newroot /newroot/oldroot
ip-10-10-12-162:~ # mount --move /oldroot/dev /dev
ip-10-10-12-162:~ # mount --move /oldroot/proc /proc
ip-10-10-12-162:~ # mount --move /oldroot/sys /sys
ip-10-10-12-162:~ # mount --move /oldroot/run /run
ip-10-10-12-162:~ # fuser -vm /oldroot
                     USER        PID ACCESS COMMAND
/oldroot:            root     kernel mount /oldroot
                     root          1 ...e. systemd
                     messagebus    868 ...e. dbus-daemon
                     root       1928 ...e. sshd
                     root       2373 ...e. sshd
                     ec2-user   2389 ...e. sshd
                     ec2-user   2391 ..ce. bash
                     root       2539 f.ce. sudo
                     root       2540 ...e. su
                     root       2541 ..ce. bash
                     root       4114 F..e. rsyslogd
                     root       4122 ...e. systemd-journal

ip-10-10-12-162:~ # for MOUNT in $(cat /proc/mounts | cut -d ' ' -f 2 | grep '/oldroot/' | sort -ru); do umount $MOUNT || true; done
                     
ip-10-10-12-162:~ # systemctl restart sshd

ip-10-10-12-162:~ # fuser -vm /oldroot
                     USER        PID ACCESS COMMAND
/oldroot:            root     kernel mount /oldroot
                     root          1 ...e. systemd
                     messagebus    868 ...e. dbus-daemon
                     root       2373 ...e. sshd
                     ec2-user   2389 ...e. sshd
                     ec2-user   2391 ..ce. bash
                     root       2539 f.ce. sudo
                     root       2540 ...e. su
                     root       2541 ..ce. bash
                     root       4114 F..e. rsyslogd
                     root       4122 ...e. systemd-journal

ip-10-10-12-162:~ # pkill --signal HUP sshd \
>   && systemctl daemon-reexec
Connection to ip-10-10-12-162.ec2.internal closed by remote host.
Connection to ip-10-10-12-162.ec2.internal closed.

localworkstation $ ssh ip-10-10-12-162.ec2.internal
Warning: Permanently added 'ip-10-10-12-162.ec2.internal' (ED25519) to the list of known hosts.
Last login: Fri Mar  1 00:26:58 2024 from 10.10.1.186
SUSE Linux Enterprise Server 15 SP4 for SAP Applications x86_64 (64-bit)

As "root" (sudo or sudo -i) use the:
  - zypper command for package management
  - yast command for configuration management

Management and Config: https://www.suse.com/suse-in-the-cloud-basics
Documentation: https://www.suse.com/documentation/sles-15/
Community: https://community.suse.com/

Have a lot of fun...
ec2-user@ip-10-10-12-162:~>


```